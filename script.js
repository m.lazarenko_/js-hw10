

const tabs = document.querySelectorAll('.tabs-title');
const tabsTexts = document.querySelectorAll("ul.tabs-content > li");




for(let i = 0; i < tabs.length; i++){
    tabs[i].addEventListener('click', (event) => {

        let tabsCurrent = event.target.parentElement.children;

        for(let j = 0; j < tabsCurrent.length; j++){
            tabsCurrent[j].classList.remove('active')
        }   

        event.target.classList.add('active')

        let contentsCurrent = event.target.parentElement.nextElementSibling.children;
        for(let k = 0; k < contentsCurrent.length; k++){
            contentsCurrent[k].classList.remove('content-active')
        }

        tabsTexts[i].classList.add('content-active')
    })

    
}
